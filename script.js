const form = document.querySelector("form");
form.addEventListener("submit", (e) => {
  e.preventDefault();
  const email = document.querySelector("#email");
  const password = document.querySelector("#password");
  const gender = document.querySelector("#gender");
  const perm = document.querySelectorAll(".perm");
  const role = document.querySelectorAll(".role");
  const perms = [];
  let rolev;
  role.forEach((e) => e.checked && (rolev = e.value));
  perm.forEach((e) => e.checked && perms.push(e.value));

  const result = {
    email: email.value,
    password: password.value,
    gender: gender.value,
    role: rolev,
    perm: perms,
  };
  console.log(result);
  console.log(passval(result.password))
  console.log(permval(result.perm))
  {passval(result.password) && permval(result.perm) && formsuccess(result)}
  
  // console.log(email.value);
  // console.log(password.value);
  // console.log(gender.value);
  // console.log(rolev);
  // console.log(perms);
});

function passval(pass) {
  const regex = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  const errordiv = document.querySelector("#passerror");
  if (pass.length < 6 || pass.length>=17) {
    errordiv.innerHTML = "the password should have atleast 6 characters and atmost 16 characters";
    return false;
  } else if (!regex.test(pass)) {
    errordiv.innerHTML = "the password must contain atleast one (A-Z,a-z,0-9)";
    return false;
  } else {
    errordiv.innerHTML = "";
    return true;
  }
}
function permval(perms) {
  const errordiv = document.querySelector("#permerror");
  if (perms.length >= 2) {
    errordiv.innerHTML = "";
    return true;
  } else {
    errordiv.innerHTML = "<p>should have atleast 2 permission</p>";
    return false;
  }
}

function formsuccess(result){
    form.remove();
    const card = document.createElement("div");   
    const body = document.querySelector("body");   
    card.classList="card" 
    const cnfbtn = document.createElement('button');
    cnfbtn.innerText="CONFIRM";    
    cnfbtn.addEventListener("click",()=>alert("Submission confirmed"))
    const key = Object.keys(result);
    key.forEach(e=>{
        const p = document.createElement("p");
        p.innerHTML=`${e} : ${result[e]}`
        card.appendChild(p)
    })   
    card.appendChild(cnfbtn)
    body.appendChild(card)

}
